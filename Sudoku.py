__author__ = 'Mohammad Javad Maleki'
import random , sys,time
################### start algorithm ###################
class SolvingSudoku:
    def __init__(self):
        self.grid = [['.' for _ in range(9)] for _ in range(9)]
        self.generate_grid()
        self.row = 0
        self.col = 0

        input("Press Enter to Solve ...")
        start = time.time()
        if self.solve():
            print ('After Solve: \n')
            self.Dispaly_Grid()
            finish = time.time() - start
            print ("Soduku solved in " + str(finish))
        else:
            print ("Soduku can not be solve!")
        input()
    def generate_grid(self): # generate random numbers in 9*9 grid by sudoku rules
        i = 0
        while(i<20):
            x =random.randint(0,8)
            y = random.randint(0,8)
            num = random.randint(1,9)
            if self.grid[x][y] is '.':
                if self.promising(x,y,num):
                    i +=1
                    self.grid[x][y] = num
        self.Dispaly_Grid()
    def Dispaly_Grid(self): # Draw the 9*9 grid
        c = 0
        for i in range(9):
            k=0
            if c is 3:
                for x in range(26):
                    sys.stdout.write("-")
                sys.stdout.write('\n')
                c = 0
            for j in range(9):
                sys.stdout.write(str(self.grid[i][j]) + " ")
                k = k + 1
                if k is 3 :
                    sys.stdout.write(" | ")
                    k=0
            sys.stdout.write('\n')
            c+=1
    def solve(self):
        if self.CheckGrid():
            return True
        for x in range(self.row,9):
            for y in range(self.col,9):
                if self.grid[x][y] is '.':
                    for num in range(1,10):
                        if self.promising(x,y,num):
                            self.grid[x][y] = num
                            self.row = x
                            self.col = 0
                            if self.solve():
                                return True
                            self.grid[x][y] = '.'
                    return False
    def CheckGrid(self):
        for i in range(9):
            for j in range(9):
                if self.grid[i][j] is '.':
                    return False
        return True
    def promising(self,x,y,num): # check num is promising at grid[x][y] or not
        # check rows
        for i in range(9):
            if num is self.grid[x][i]:
                if y is i:
                    pass
                else:
                    return False

        #ckeck coloums
        for i in range(9):
            if num is self.grid[i][y]:
                if x is i:
                    pass
                else:
                    return False
        # ckeck blocks --> temp is a 3*3 array that a block stores on it.
        self.temp = [['.' for _ in range(3)] for _ in range(3)]
        ###### Conditions :
        if (x >= 0 and x <3) and (y >= 0 and y<3):
            self.fill_temp(0,0)
            if not self.CheckIsBoxSafe(x,y,num):
                return False
        elif (x >= 0 and x <3) and (y >= 3 and y<6):
            self.fill_temp(0,3)
            if not self.CheckIsBoxSafe(x,y,num):
                return False
        elif (x >= 0 and x <3) and (y >= 6 and y<9):
            self.fill_temp(0,6)
            if not self.CheckIsBoxSafe(x,y,num):
                return False
        elif (x >= 3 and x <6) and (y >= 0 and y<3):
            self.fill_temp(3,0)
            if not self.CheckIsBoxSafe(x,y,num):
                return False
        elif (x >= 3 and x <6) and (y >= 3 and y<6):
            self.fill_temp(3,3)
            if not self.CheckIsBoxSafe(x,y,num):
                return False
        elif (x >= 3 and x <6) and (y >= 6 and y<9):
            self.fill_temp(3,6)
            if not self.CheckIsBoxSafe(x,y,num):
                return False
        elif (x >= 6 and x <9) and (y >= 0 and y<3):
            self.fill_temp(6,0)
            if not self.CheckIsBoxSafe(x,y,num):
                return False
        elif (x >= 6 and x <9) and (y >= 3 and y<6):
            self.fill_temp(6,3)
            if not self.CheckIsBoxSafe(x,y,num):
                return False
        elif (x >= 6 and x <9) and (y >= 6 and y<9):
            self.fill_temp(6,6)
            if not self.CheckIsBoxSafe(x,y,num):
                return False

        return True
    def CheckIsBoxSafe(self,row,col,num): # check if num in Block that now is in temp array is safe or not
        for i in range(3):
            for j in range(3):
                if (row%3) is i and (col%3) is j:
                    pass
                else:
                    if self.temp[i][j] is num:
                        return False
        return True
    def fill_temp(self,minRow,minCol): # Fill the 3*3 temp array with one of the blocks in 9*9 grid
        tempcol = minCol
        for row in range(3):
            for col in range(3):
                self.temp[row][col] = self.grid[minRow][tempcol]
                tempcol +=1
            minRow +=1
            tempcol = minCol

if __name__ == '__main__':
    sudoku = SolvingSudoku()

