from random import Random, random
import math
class Queen():
    populationSize =100
    population=[]
    Percent={}
    n=8
     
    def quickSort(self,alist):
        self.quickSortHelper(alist,0,len(alist)-1)
    def quickSortHelper(self,alist,first,last):
        if first<last:
           splitpoint = self.partition(alist,first,last)
           self.quickSortHelper(alist,first,splitpoint-1)
           self.quickSortHelper(alist,splitpoint+1,last)
    def partition(self,alist,first,last):
       pivotvalue = alist[first][1]
       leftmark = first+1
       rightmark = last
       done = False
       while not done:
           while leftmark <= rightmark and alist[leftmark][1] <= pivotvalue:
              leftmark = leftmark + 1
           while alist[rightmark][1] >= pivotvalue and rightmark >= leftmark:
              rightmark = rightmark -1
           if rightmark < leftmark:
              done = True
           else:
              temp = alist[leftmark]
              alist[leftmark] = alist[rightmark]
              alist[rightmark] = temp
       temp = alist[first]
       alist[first] = alist[rightmark]
       alist[rightmark] = temp
       return rightmark


    def fitness(self,List):
          counter=0
          for i in range(self.n):
            for j in range(self.n):
                if(i!=j):
                    if(List[i]==List[j] or math.fabs(List[i]-List[j])==math.fabs(i-j)):
                       counter+=1
          return counter/2
    def Calculation_of_percentage(self):
          self.Percent={}
          self.populationSize=len(self.population)
          for x in range(self.populationSize):
               self.Percent[x]=(self.population[x],self.fitness(self.population[x]))
          self.quickSort(self.Percent)
    def Mutation(self):
        r = Random()
        List=[]
        Ra=r.randint(1,196)
        while (len(List)!=Ra):
              Ra1=r.randint(100,295)
              if Ra1 not in List:
               Ra2=r.randint(0,self.n-1)
               Ra3=r.randint(0,self.n-1)
               self.population[Ra1][Ra2]=Ra3
               List.append(Ra1)
         
    def Crossover(self,L1,L2):
         r = Random()
         Ra=r.randint(1,self.n)
         temp1=[]
         temp2=[]
         for i in range(self.n):
               temp1.append(self.population[L1][i])
               temp2.append(self.population[L2][i])






         for i in range(Ra):
             temp=temp1[i]
             temp1[i]=temp2[i]
             temp2[i]=temp
         self.population.append(temp1)
         self.population.append(temp2)         
    def Copy(self):
        self.population=[]
        for x in range(self.populationSize):
            self.population.append(self.Percent[x][0])
    def Format(self):
        for i in range(295,99,-1):
            del self.population[i]
        self.populationSize=100
        
def main():
    r = Random()
    Queen1=Queen()
    st=""
    flag=0
    Queen1.population= [[r.randint(0,Queen1.n-1) for y in range(Queen1.n)] for x in range(Queen1.populationSize)]
    while(1!=0):
       Queen1.Calculation_of_percentage()
       Queen1.Copy()
       for i in range(len(Queen1.Percent)):
           if(Queen1.Percent[i][1] == 0):
              print(Queen1.Percent[i][0])
              for j in range(Queen1.n-1,-1,-1):
                 for k in range(Queen1.n):
                     if(Queen1.Percent[i][0][k]==j):
                         st+=' Q'
                     else:
                         st+=' *'
                 st+='\n'
              print(st)
              flag=1
              break
       if(flag==1):
           break
       for x in range(Queen1.populationSize-2):
           Queen1.Crossover(x,x+1)           
       Queen1.Mutation()
       Queen1.Calculation_of_percentage()
       Queen1.Copy()
       if(len(Queen1.population)!=100):
         Queen1.Format()
main()
print("end")


