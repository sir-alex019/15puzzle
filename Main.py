__author__ = 'NeginAshrafi'
import time
strategy = {'one': ['two', 'five'], 'two': ['one', 'three', 'six'], 'three': ['two','four', 'seven'], 'four': ['three', 'eight'],
            'five': ['one', 'six', 'nine'], 'six': ['two', 'five', 'seven', 'ten'], 'seven': ['three', 'six', 'eight', 'eleven'],
            'eight': ['four', 'seven', 'twelve'], 'nine': ['five','ten','thirteen'], 'ten': ['six', 'nine', 'eleven', 'fourteen'],
            'eleven': ['seven', 'ten', 'twelve', 'fifteen'], 'twelve': ['eight', 'eleven', 'sixteen'], 'thirteen': ['nine', 'fourteen'],
            'fourteen': ['ten', 'thirteen', 'fifteen'],'fifteen': ['eleven', 'fourteen', 'sixteen' ], 'sixteen' : ['twelve', 'fifteen']}
node = ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine',
        'ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen']


class BFS:
    def __init__(self, _start, _goal, _currentState, parent):
        self.goal = _goal
        self.queue = [(_start, _currentState, parent)]
    def isEmpty(self):
        return self.queue == []

    def enqueue(self, item):
        self.queue.insert(0,item)

    def dequeue(self):
        return self.queue.pop()

    def size(self):
        return len(self.queue)



class IDS:
    def __init__(self, _start, _goal, _currentState, _currentLimit,parent):
        self.goal = _goal
        self.stack = [(_start, _currentState, parent)]
        self.limit =_currentLimit

    def getCurrentLimit(self):
        return self.limit

    def isEmpty(self):
        return self.stack == []
    def push(self, item):
        self.stack.append(item)
    def pop(self):
        return self.stack.pop()
    def size(self):
        return len(self.stack)

def readfile(_namefile):
    _startNode = {}
    _lines = []
    _startFile = open(_namefile)
    for _line in _startFile:
        _lines.append(_line.replace('\n', ''))
    for i in range(0, len(_lines), 1):
        _temp = _lines[i].split(' ')
        j = 0
        while j < len(_temp):
            _jTemp = j + i * 4
            _startNode[node[_jTemp]] = _temp[j]
            if _namefile == "start.txt":
                if _temp[j] == '*':
                    _currentState = node[_jTemp]
            j += 1


    if _namefile == "start.txt":
        _values = {'startNode':_startNode, 'currentState': _currentState}
    else:
        _values = _startNode
    return _values;

def readInput(_typeInput):
    _startNode = {}
    for i in range(0,16,1):
        print(node[i],": ")
        _startNode[node[i]] = input()
        if _typeInput == "start":
            if  _startNode[node[i]] == '*':
                _currentState = node[i]


    if _typeInput == "start":
        _values = {'startNode':_startNode, 'currentState': _currentState}
    else:
        _values = _startNode
    return _values;

def main():
    while True:
        _choose_Item = eval(input("Select a item:\n1.Enter manual\n2.Read a file\n3.Exit"))

        if _choose_Item == 1:
            print("Enter Start Node: ")
            _start = readInput("start")
            print("Enter Goal Node: ")
            _goal = readInput("goal")

        elif _choose_Item == 2:
            _start = readfile("start.txt");
            _goal = readfile("goal.txt")

        elif _choose_Item == 3:
            break;

        while True:
            _choose_Way = eval(input("Select a way:\n1.BFS\n2.IDS\n3.Back"))
            start_time = time.time()

            if _choose_Way == 1:
                bfsqueue = BFS(_start['startNode'],_goal,_start['currentState'], None)
                while(True):
                    item = bfsqueue.dequeue()
                    for i in range(0,16,4):
                        print(item[0][node[i]]," ",item[0][node[i+1]]," ",item[0][node[i+2]]," ",item[0][node[i+3]])
                    print("------------------------\n")
                    if _goal == item[0]:
                        elapsed_time = time.time() - start_time
                        print("Duration: ",elapsed_time)
                        break;
                    for i in range(0,len(strategy[item[1]]),1):
                        temp = item[0].copy()
                        tempnode = temp[strategy[item[1]][i]]
                        temp[strategy[item[1]][i]] = '*'
                        temp[item[1]]= tempnode
                        if temp != item[2]:
                            bfsqueue.enqueue((temp,strategy[item[1]][i], item[0]))

            elif _choose_Way == 2:
                _currentlimit = 0
                idsstack = IDS(_start['startNode'],_goal,_start['currentState'], _currentlimit, None)
                _counter = 0
                _numtopstack = -1
                while(True):
                    if idsstack.isEmpty():
                        _currentlimit += 1
                        _counter = 0
                        _numtopstack = -1
                        print("limit : ", _currentlimit)
                        idsstack = IDS(_start['startNode'],_goal,_start['currentState'],_currentlimit, None)
                    if _numtopstack == 0:
                        _counter -= 1
                    print("level = " , _counter)
                    item = idsstack.pop()
                    for i in range(0,16,4):
                        print(item[0][node[i]]," ",item[0][node[i+1]]," ",item[0][node[i+2]]," ",item[0][node[i+3]])
                    _numtopstack -= 1
                    if _goal == item[0]:
                        elapsed_time = time.time() - start_time
                        print("Duration: ",elapsed_time)
                        break;
                    if(_counter < idsstack.getCurrentLimit()):
                        _numtopstack = len(strategy[item[1]])

                        for i in range(0,len(strategy[item[1]]),1):
                            temp = item[0].copy()
                            tempnode = temp[strategy[item[1]][i]]
                            temp[strategy[item[1]][i]] = '*'
                            temp[item[1]]= tempnode
                            if temp != item[2]:
                                idsstack.push((temp,strategy[item[1]][i],item[0]))
                            elif temp == item[2]:
                                _numtopstack -= 1
                        _counter += 1

            elif _choose_Way == 3:
                break;

if __name__ == "__main__": main()
